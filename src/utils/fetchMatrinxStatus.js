import _ from 'lodash';

export default async () => {
  const fetchData = await fetch(
    'https://cmre.cpn.co.th/webapi/master/getunit?projectid=CDLA14',
    {
      method: 'POST'
    }
  )
    .then(response => response.json())
    .then((res) => {
      const data = _.chain(_.get(res, 'data', []))
        .reduce((result, value) => {
          return {
            ...result,
            [value.UnitNumber]: value.UnitStatus
          };
        }, {})
        .value();
      return data;
    })
    .catch(err => err);
  return fetchData;
};
