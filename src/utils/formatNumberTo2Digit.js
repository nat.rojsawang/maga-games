import _ from 'lodash';

export default (num) => {
  return _.padStart(num, 2, '0');
};
