import RNFetchBlob from 'rn-fetch-blob';
import _ from 'lodash';

const { fs } = RNFetchBlob;
export const PathFileMedias = `${fs.dirs.DocumentDir}/content`;

export const getMediaPath = (filename) => {
  return `${PathFileMedias}/${filename}`;
};

const getFileOffline = async (url, title, size) => {
  let fileDownload = true;
  const fileExists = await fs.exists(getMediaPath(title));
  if (fileExists) {
    const fileSize = await fs.stat(getMediaPath(title)).then((stats) => {
      return stats.size;
    });
    fileDownload = fileSize !== size;
  }

  return new Promise((resolve, reject) => {
    resolve('res.path()');
    if (fileDownload) {
      RNFetchBlob.config({
        fileCache: false,
        path: getMediaPath(title)
      })
        .fetch('GET', url)
        .then((res) => {
          console.log('The file saved to ', res.info().status);
          const status = res.info().status;
          if (status === 200) {
            console.log(
              'The file saved to ',
              res.path(),
              `${PathFileMedias}/${title}`
            );
            resolve(res.path());
          }
        })
        .catch(reject);
    } else {
      console.log('file exists');
      resolve('file exists');
    }
  });
};

const sleep = (timmer) => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve(true);
    }, timmer);
  });
};
export const downloadFiles = async (fileList) => {
  const lists = _.chunk(fileList, 10);
  for (const list of lists) {
    const downloadFilePromise = _.map(list, (o) => {
      return getFileOffline(o.url, o.filename, _.get(o, ['metadata', 'size']));
    });
    await Promise.all(downloadFilePromise);
    await sleep(1000);
  }
};

export const deleteFiles = async (fileList) => {
  const lists = _.chunk(fileList, 10);
  for (const list of lists) {
    const deleteFilePromise = _.map(list, (o) => {
      console.log({ o });
      return fs
        .unlink(`${PathFileMedias}/${o.filename}`)
        .then(() => {
          console.log('done');
        })
        .catch(() => {
          console.log('fail');
        });
    });
    await Promise.all(deleteFilePromise);
    await sleep(1000);
  }
};
