import React, { useState } from 'react';
import _ from 'lodash';

import {
  Block,
  Text
} from './style';

import { getMediaPath } from '../../utils/fileManager';

export default () => {
  const [navActiveIndex, setNavActiveIndex] = useState(0);
  return (
    <Block>
      <Text>screen</Text>
    </Block>
  );
};
