import styled from 'styled-components';

export const Block = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  height: 100%;
`;

export const Text = styled.Text`
  font-family: 'HelveticaNeue-UltraLight';
  font-size: 60;
  transform: scaleY(1.75);
  color: #000;
`;
